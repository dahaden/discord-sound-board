import React from 'react';
import ReactDOM from 'react-dom';
import createBrowserHistory from 'history/createBrowserHistory'
import Routes from './routes';
import './index.css';

const broswerHistory = createBrowserHistory();

ReactDOM.render(
  <Routes history={broswerHistory} />,
  document.getElementById('root')
);