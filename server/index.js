'use strict';

const app = require('./app');
const discord = require('./discord');

const PORT = process.env.PORT || 9000;

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});

discord.start();