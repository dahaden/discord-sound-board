const Discord = require('discord.js');
const client = new Discord.Client();
const textMap = require('../soundMap');
const discordAppinfo = require('../env').discord;
const emoji = require('node-emoji');
const path = require('path');
const queue = require('queue');
// const express = require('express')
// const app = express();
//var session = require('express-session');

// const config = {
//   discord: {
//     client_id: '345358566808354817',
//     client_secret: 'x2SqweOJeYRcvMW_RFI1uwQUzgJpOU97',
//     redirect_uri: 'https://dhaden.au.ngrok.io/oauth2'
//   }
// };

//require('express-oauth2')(app, config);

// app.use(session({
//   secret: 'keyboard cat',
//   resave: true,
//   saveUninitialized: true
// }));

// app.post('/oauth2', function (req, res) {
//   console.log(req.body);
//   res.status(204).send();
// })

// app.listen(3000, function () {
//   console.log('Example app listening on port 3000!')
// })

function start() {
  const q = queue({
    concurrency: 1,
    timeout: 5000,
    autostart: true
  });

  client.on('ready', () => {
    console.log('I am ready!');
    client.channels.forEach(channel => {
      if (channel.type === 'text') {
        channel.send(`Hi everybody #${channel.name}`)
      }
    });
  });

  client.on('message', message => {
    const shortCut = getShortcut(message);
    if (shortCut) {
      if (shortCut.soundFile) {
        const vChannel = findVoiceChannelUserIsIn(message.author);
        if (vChannel && vChannel.joinable) {
            q.push(() => {
              return vChannel.join()
                .then(connection => {
                  return new Promise(resolve => {
                    const streamDispatch = connection.playFile(path.resolve(path.join(textMap.soundsDir, shortCut.soundFile)));
                    streamDispatch.on('end', () => {
                      resolve();
                    });
                  });
            });
          });
        }
      } else {
        message.channel.send(shortCut.text, {tts: true});
      }
    }
  });

  function getShortcut(message) {
    return textMap.aoe2Shortcuts.find(shortcut => shortcut.id === message.content);
  }

  function findVoiceChannelUserIsIn(user) {
    return client.channels.find(channel => {
        if (channel.type === 'voice') {
          const ids = channel.members.map(u => u.id);
          if (ids.includes(user.id)) {
            return true;
          }
        }
        return false;
      });
  }

  client.login(discordAppinfo.token);
}
module.exports = { start };