FROM node:7

RUN npm install -g ffmpeg-binaries

COPY ./ /var/sound-board/

WORKDIR /var/sound-board/

RUN npm install
RUN npm run build

CMD node server